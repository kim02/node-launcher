{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "thornode-daemon.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "thornode-daemon.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "thornode-daemon.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Common labels
*/}}
{{- define "thornode-daemon.labels" -}}
helm.sh/chart: {{ include "thornode-daemon.chart" . }}
{{ include "thornode-daemon.selectorLabels" . }}
app.kubernetes.io/version: {{ include "thornode-daemon.tag" . | default .Chart.AppVersion | quote }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
app.kubernetes.io/net: {{ include "thornode-daemon.net" . }}
app.kubernetes.io/type: {{ .Values.type }}
{{- end -}}

{{/*
Selector labels
*/}}
{{- define "thornode-daemon.selectorLabels" -}}
app.kubernetes.io/name: {{ include "thornode-daemon.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end -}}

{{/*
Create the name of the service account to use
*/}}
{{- define "thornode-daemon.serviceAccountName" -}}
{{- if .Values.serviceAccount.create -}}
    {{ default (include "thornode-daemon.fullname" .) .Values.serviceAccount.name }}
{{- else -}}
    {{ default "default" .Values.serviceAccount.name }}
{{- end -}}
{{- end -}}

{{/*
Net
*/}}
{{- define "thornode-daemon.net" -}}
{{- default .Values.net .Values.global.net -}}
{{- end -}}


{{/*
RPC Port
*/}}
{{- define "thornode-daemon.rpc" -}}
{{- if eq (include "thornode-daemon.net" .) "mainnet" -}}
    {{ .Values.service.port.mainnet.rpc}}
{{- else if eq (include "thornode-daemon.net" .) "stagenet" -}}
    {{ .Values.service.port.stagenet.rpc}}
{{- else -}}
    {{ .Values.service.port.testnet.rpc }}
{{- end -}}
{{- end -}}

{{/*
P2P Port
*/}}
{{- define "thornode-daemon.p2p" -}}
{{- if eq (include "thornode-daemon.net" .) "mainnet" -}}
    {{ .Values.service.port.mainnet.p2p}}
{{- else if eq (include "thornode-daemon.net" .) "stagenet" -}}
    {{ .Values.service.port.stagenet.p2p}}
{{- else -}}
    {{ .Values.service.port.testnet.p2p }}
{{- end -}}
{{- end -}}

{{/*
ETH Router contract
*/}}
{{- define "thornode-daemon.ethRouterContract" -}}
{{- if eq (include "thornode-daemon.net" .) "mainnet" -}}
    {{ .Values.ethRouterContract.mainnet }}
{{- else if eq (include "thornode-daemon.net" .) "stagenet" -}}
    {{ .Values.ethRouterContract.stagenet }}
{{- else -}}
    {{ .Values.ethRouterContract.testnet }}
{{- end -}}
{{- end -}}

{{/*
chain id
*/}}
{{- define "thornode-daemon.chainID" -}}
{{- if eq (include "thornode-daemon.net" .) "mainnet" -}}
    {{ .Values.chainID.mainnet}}
{{- else if eq (include "thornode-daemon.net" .) "stagenet" -}}
    {{ .Values.chainID.stagenet}}
{{- else -}}
    {{ .Values.chainID.testnet }}
{{- end -}}
{{- end -}}
