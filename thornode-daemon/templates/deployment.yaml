apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "thornode-daemon.fullname" . }}
  labels:
    {{- include "thornode-daemon.labels" . | nindent 4 }}
spec:
  replicas: {{ .Values.replicaCount }}
  strategy:
    type: {{ .Values.strategyType }}
  selector:
    matchLabels:
      {{- include "thornode-daemon.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      labels:
        {{- include "thornode-daemon.selectorLabels" . | nindent 8 }}
    spec:
    {{- with .Values.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
    {{- end }}
      serviceAccountName: {{ include "thornode-daemon.serviceAccountName" . }}
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}

      {{- if .Values.priorityClassName }}
      priorityClassName: {{ .Values.priorityClassName }}
      {{- end }}

      initContainers:
      {{- if .Values.haltHeight }}
      - name: halt-height
        image: {{ include "thornode-daemon.image" . }}
        imagePullPolicy: {{ .Values.image.pullPolicy }}
        command: ['sh', '-c', '[ ! -e /root/.thornode/data/priv_validator_state.json ] || [ $(cat /root/.thornode/data/priv_validator_state.json | jq -r .height) -lt {{ .Values.haltHeight }} ] || sleep infinity']
        volumeMounts:
          - name: data
            mountPath: /root/
      {{- end }}

      {{- if .Values.recover }}
      - name: ninerealms
        image: google/cloud-sdk@sha256:f94bacf262ad8f5e7173cea2db3d969c43b938a036e3c6294036c3d96261f2f4
        command: ['/scripts/ninerealms.sh']
        volumeMounts:
          - name: scripts
            mountPath: /scripts
          - name: data
            mountPath: /root/
      {{- end }}

      {{- if default .Values.peer }}
      - name: init-peer
        image: busybox:{{ .Values.global.images.busybox.tag }}@sha256:{{ .Values.global.images.busybox.hash }}
        command: ['sh', '-c', 'until nc -zv {{ default .Values.peer .Values.global.peer }}:{{ include "thornode-daemon.rpc" . }}; do echo waiting for peer thornode; sleep 2; done']
      {{- end }}

      {{- if .Values.peerApi }}
      - name: init-peer-api
        image: busybox:{{ .Values.global.images.busybox.tag }}@sha256:{{ .Values.global.images.busybox.hash }}
        command: ['sh', '-c', "until nc -zv {{ .Values.peerApi }}:1317; do echo waiting for peer thornode; sleep 2; done"]
      {{- end }}
      containers:
        - name: {{ .Chart.Name }}
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: {{ .Values.image.repository }}:{{ .Values.image.tag }}@sha256:{{ .Values.image.hash }}

          imagePullPolicy: {{ .Values.image.pullPolicy }}
          command:
            -  /scripts/fullnode.sh
          volumeMounts:
            - name: data
              mountPath: /root/
            - name: scripts
              mountPath: /kube-scripts/
          env:
            - name: VALIDATOR
              value: "false"
            - name: PEER
              value: {{ default .Values.peer }}
            - name: PEER_API
              value: {{ .Values.peerApi }}
            - name: DEBUG
              value: "{{ .Values.debug }}"
            - name: HARDFORK_BLOCK_HEIGHT
              value: "{{ .Values.haltHeight }}"
            - name: NET
              value: {{ include "thornode-daemon.net" . }}
            - name: SIGNER_NAME
              value: {{ .Values.signer.name }}
            - name: SIGNER_PASSWD
              value: {{ .Values.signer.password }}
            - name: CHAIN_ID
              value: {{ include "thornode-daemon.chainID" . }}
            - name: THOR_AUTO_STATE_SYNC_ENABLED
              value: "{{ .Values.statesync.auto }}"
            - name: THOR_COSMOS_STATE_SYNC_SNAPSHOT_INTERVAL
              value: "{{ .Values.statesync.snapshotInterval }}"
            - name: THOR_TENDERMINT_LOG_FORMAT
              value: "json"
            {{- range $key, $value := .Values.env }}
            - name: {{ $key }}
              value: {{ $value | quote }}
            {{- end }}
          ports:
            - name: api
              containerPort: {{ .Values.service.port.api }}
              protocol: TCP
            - name: grpc
              containerPort: {{ .Values.service.port.grpc }}
              protocol: TCP
            - name: p2p
              containerPort: {{ include "thornode-daemon.p2p" . }}
              protocol: TCP
            - name: rpc
              containerPort: {{ include "thornode-daemon.rpc" . }}
              protocol: TCP
            - name: prometheus
              containerPort: 26660
              protocol: TCP
          livenessProbe:
            timeoutSeconds: 10
            httpGet:
              path: /status
              port: rpc
          startupProbe:
            failureThreshold: 30
            timeoutSeconds: 10
            httpGet:
              path: /status
              port: rpc
          readinessProbe:
            timeoutSeconds: 10
            httpGet:
              path: /status
              port: rpc
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
      volumes:
      - name: data
      {{- if and .Values.persistence.enabled (not .Values.persistence.hostPath) }}
        persistentVolumeClaim:
          claimName: {{ if .Values.persistence.existingClaim }}{{ .Values.persistence.existingClaim }}{{- else }}{{ template "thornode-daemon.fullname" . }}{{- end }}
      {{- else if and .Values.persistence.enabled .Values.persistence.hostPath }}
        hostPath:
          path: {{ .Values.persistence.hostPath }}
          type: DirectoryOrCreate
      {{- else }}
        emptyDir: {}
      {{- end }}
      - name: scripts
        configMap:
          name: {{ include "thornode-daemon.fullname" . }}-scripts
          defaultMode: 0777
      - name: configs
        configMap:
          name: {{ include "thornode-daemon.fullname" . }}-configs
          defaultMode: 0666
      {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
    {{- with .Values.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
    {{- end }}
    {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
    {{- end }}
