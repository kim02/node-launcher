#!/usr/bin/env bash

source ./scripts/core.sh

if [ "$TYPE" != "daemons" ]; then
  get_node_info_short

  if ! node_exists; then
    die "No existing MAYANode found, make sure this is the correct name"
  fi
fi

display_status

echo -e "=> Destroying a $boldgreen$TYPE$reset MAYANode on $boldgreen$NET$reset named $boldgreen$NAME$reset"
echo
echo

if [ "$TYPE" != "daemons" ]; then
  warn "!!! Make sure you got your BOND back before destroying your MAYANode !!!"
else
  warn "!!! Make sure no validators are using these daemons before destroying them !!!"
fi

confirm
echo "=> Deleting MAYANode"
helm delete "$NAME" -n "$NAME"
kubectl delete namespace "$NAME"
