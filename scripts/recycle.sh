#!/usr/bin/env bash

set -euo pipefail

source ./scripts/core.sh

get_node_info

if ! node_exists; then
  die "No existing MAYANode found, make sure this is the correct name"
fi

if [ "$TYPE" != "validator" ]; then
  die "Only validators should be recycled"
fi

display_status

echo -e "=> Recycling a $boldgreen$TYPE$reset MAYANode on $boldgreen$NET$reset named $boldgreen$NAME$reset"
echo
echo
warn "!!! Make sure your got your BOND back before recycling your MAYANode !!!"
confirm

# delete gateway resources
echo -e "$beige=> Recycling MAYANode - deleting gateway resources...$reset"
kubectl -n "$NAME" delete deployment gateway
kubectl -n "$NAME" delete service gateway
kubectl -n "$NAME" delete configmap gateway-external-ip

# delete mayanode resources
echo -e "$beige=> Recycling MAYANode - deleting mayanode resources...$reset"
kubectl -n "$NAME" delete deployment mayanode
kubectl -n "$NAME" delete configmap mayanode-external-ip
kubectl -n "$NAME" delete secret mayanode-password
kubectl -n "$NAME" delete secret mayanode-mnemonic

# delete all key material from mayanode while preserving chain data
echo -e "$beige=> Recycling MAYANode - deleting mayanode derived keys...$reset"
IMAGE=alpine:latest@sha256:4edbd2beb5f78b1014028f4fbb99f3237d9561100b6881aabbf5acce2c4f9454
SPEC=$(
  cat <<EOF
{
  "apiVersion": "v1",
  "spec": {
    "containers": [
      {
        "command": [
          "rm",
          "-rf",
          "/root/.mayanode/MAYAChain-ED25519",
          "/root/.mayanode/data/priv_validator_state.json",
          "/root/.mayanode/keyring-file/",
          "/root/.mayanode/config/node_key.json",
          "/root/.mayanode/config/priv_validator_key.json",
          "/root/.mayanode/config/genesis.json"
        ],
        "name": "reset-mayanode-keys",
        "stdin": true,
        "tty": true,
        "image": "$IMAGE",
        "volumeMounts": [{"mountPath": "/root", "name":"data"}]
      }
    ],
    "volumes": [{"name": "data", "persistentVolumeClaim": {"claimName": "mayanode"}}]
  }
}
EOF
)
kubectl -n "$NAME" run -it --rm reset-mayanode-keys --restart=Never --image="$IMAGE" --overrides="$SPEC"

# delete bifrost resources
echo -e "$beige=> Recycling MAYANode - deleting bifrost resources...$reset"
kubectl -n "$NAME" delete deployment bifrost
kubectl -n "$NAME" delete pvc bifrost
kubectl -n "$NAME" delete configmap bifrost-external-ip

# recreate resources
echo -e "$green=> Recycling MAYANode - recreating deleted resources...$reset"
NET=$NET TYPE=$TYPE NAME=$NAME ./scripts/install.sh

echo -e "$green=> Recycle complete.$reset"
