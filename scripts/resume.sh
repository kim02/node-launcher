#!/usr/bin/env bash

set -e

source ./scripts/core.sh

get_node_info_short

echo "=> Resuming node global halt from a MAYANode named $boldyellow$NAME$reset"
confirm

kubectl exec -it -n "$NAME" -c mayanode deploy/mayanode -- /kube-scripts/resume.sh
sleep 5
echo MAYAChain resumed

display_status
